# eyespot
*A Java tool that helps doctors find bright differences (spots) in patients eyes*
> This project is no longer under active development

## All you need is [here](http://sirfoga.github.io/2016/04/27/eyespot)
I've written a blog post about the problem and the solution found.
However, if you wanna rush for the program, please download it [here](out/artifacts/eyespot_jar/eyespot.jar?raw=true).

## Thanks to
- Giuliana Frizziero
- Luisa Frizziero
