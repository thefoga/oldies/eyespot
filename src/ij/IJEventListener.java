/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij;

	/** Plugins that implement this interface are notified when the user
	     changes the foreground color, changes the background color,
	     closes the color picker, closes the Log window or switches to
	     another tool.
	*/
	public interface IJEventListener {
		public static final int FOREGROUND_COLOR_CHANGED = 0;
		public static final int BACKGROUND_COLOR_CHANGED = 1;
		public static final int COLOR_PICKER_CLOSED= 2;
		public static final int LOG_WINDOW_CLOSED= 3;
		public static final int TOOL_CHANGED= 4;

	public void eventOccurred(int eventID);

}
