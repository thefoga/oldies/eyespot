/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin;

import com.stefano.ui.util.AboutDialog;

/** This plugin implements the Help/About ImageJ command by opening
	the about.jpg in ij.jar, scaling it 400% and adding some text. */
public class AboutBox implements PlugIn {

	public void run(String arg) {
		StringBuilder aboutContent = new StringBuilder();
		aboutContent.append("<html><font face='Arial'>");
		aboutContent.append("<h1><b>" + "Eyespot" + "</b></h1>" +
				"<h2>A Java tool that helps doctors<br>spot bright differences in patients eyes.</h2>" +
				"<br>Version 1.0<br>Copyright (c) 2016<br>" +
				"<b>Stefano Fogarollo</b>. All Rights Reserved.<br>" +
				"Unauthorized copying of this file, via any medium is strictly prohibited.<br><br>Contact me at: sirfoga@protonmail.ch");
		aboutContent.append("</html>");

		String title = "About Eyespot";
		AboutDialog aboutDialog = new AboutDialog(aboutContent.toString(), title);
		aboutDialog.show();
	}
}
