/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin;

import ij.IJ;

import java.applet.Applet;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class BrowserLauncher implements PlugIn {
	/** The com.apple.mrj.MRJFileUtils class */
	private static Class mrjFileUtilsClass;
	/** The openURL method of com.apple.mrj.MRJFileUtils */
	private static Method openURL;
	private static boolean error;
	static {loadClasses();}


	/** Opens the specified URL (default is the ImageJ home page). */
	public void run(String theURL) {
		if (error) return;
		if (theURL==null || theURL.equals(""))
			theURL = IJ.URL;
		Applet applet = IJ.getApplet();
		if (applet!=null) {
			try {
				applet.getAppletContext().showDocument(new URL(theURL), "_blank" );
			} catch (Exception e) {}
			return;
		}
		try {openURL(theURL);}
		catch (IOException e) {}
	}

	/**
	 * Attempts to open the default web browser to the given URL.
	 * @param url The URL to open
	 * @throws IOException If the web browser could not be located or does not run
	 */
	public static void openURL(String url) throws IOException {
		String errorMessage = "";
		if (IJ.isMacOSX()) {
			if (IJ.isJava16())
				IJ.runMacro("exec('open', '"+url+"')");
			else {
				try {
					Method aMethod = mrjFileUtilsClass.getDeclaredMethod("sharedWorkspace", new Class[] {});
					Object aTarget = aMethod.invoke( mrjFileUtilsClass, new Object[] {});
					openURL.invoke(aTarget, new Object[] { new java.net.URL( url )}); 
				} catch (Exception e) {
					errorMessage = ""+e;
				}
			}
		} else if (IJ.isWindows()) {
			String cmd = "rundll32 url.dll,FileProtocolHandler " + url;
			if (System.getProperty("os.name").startsWith("Windows 2000"))
				cmd = "rundll32 shell32.dll,ShellExec_RunDLL " + url;
			Process process = Runtime.getRuntime().exec(cmd);
			// This avoids a memory leak on some versions of Java on Windows.
			// That's hinted at in <http://developer.java.sun.com/developer/qow/archive/68/>.
			try {
				process.waitFor();
				process.exitValue();
			} catch (InterruptedException ie) {
				throw new IOException("InterruptedException while launching browser: " + ie.getMessage());
			}
		} else {
				// Assume Linux or Unix
				// Based on BareBonesBrowserLaunch (http://www.centerkey.com/java/browser/)
				// The utility 'xdg-open' launches the URL in the user's preferred browser,
				// therefore we try to use it first, before trying to discover other browsers.
				String[] browsers = {"xdg-open", "netscape", "firefox", "konqueror", "mozilla", "opera", "epiphany", "lynx" };
				String browserName = null;
				try {
					for (int count=0; count<browsers.length && browserName==null; count++) {
						String[] c = new String[] {"which", browsers[count]};
						if (Runtime.getRuntime().exec(c).waitFor()==0)
							browserName = browsers[count];
					}
					if (browserName==null)
						ij.IJ.error("BrowserLauncher", "Could not find a browser");
					else
						Runtime.getRuntime().exec(new String[] {browserName, url});
				} catch (Exception e) {
					throw new IOException("Exception while launching browser: " + e.getMessage());
				}
		}
	}
	
	/**
	 * Called by a static initializer to load any classes, fields, and methods 
	 * required at runtime to locate the user's web browser.
	 */
	private static void loadClasses() {
		if (IJ.isMacOSX() && !IJ.isJava16() && IJ.getApplet()==null) {
			try {
				if (new File("/System/Library/Java/com/apple/cocoa/application/NSWorkspace.class").exists()) {
					ClassLoader classLoader = new URLClassLoader(new URL[]{new File("/System/Library/Java").toURL()});
					mrjFileUtilsClass = Class.forName("com.apple.cocoa.application.NSWorkspace", true, classLoader);
				} else
					mrjFileUtilsClass = Class.forName("com.apple.cocoa.application.NSWorkspace");
				openURL = mrjFileUtilsClass.getDeclaredMethod("openURL", new Class[] { java.net.URL.class });
			} catch (Exception e) {
				IJ.log("BrowserLauncher"+e);
				error = true;
			}
		}
	}

}

