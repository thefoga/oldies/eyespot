/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin.eyespot;

import ij.plugin.filter.*;
import ij.*;
import ij.gui.*;
import ij.process.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;


public class SpotFolder implements ExtendedPlugInFilter, DialogListener {
    private static double tolerance = 10;  /** maximum height difference between points that are not counted as separate maxima */
    private static boolean excludeOnEdges;  /** whether to exclude maxima at the edge of the image*/
    private static boolean useMinThreshold;  /** whether to accept maxima only in the thresholded height range*/
    private static final boolean lightBackground = false;  /** whether to find darkest points on light background */
    private ImagePlus imp; // the ImagePlus of the setup call
    private int flags = DOES_ALL|NO_CHANGES|NO_UNDO;// the flags (see interfaces PlugInFilter & ExtendedPlugInFilter)
    private boolean thresholded;                  // whether the input image has a threshold
    private boolean roiSaved;                     // whether the filter has changed the roi and saved the original roi
    private boolean previewing;                   // true while dialog is displayed (processing for preview)
    private Vector checkboxes;                   // a reference to the Checkboxes of the dialog
    private boolean thresholdWarningShown = false;  // whether the warning "can't find minima with thresholding" has been shown
    private Label messageArea;                  // reference to the textmessage area for displaying the number of maxima
    private int nPasses = 0;                  // for progress bar, how many images to process (sequentially or parallel threads)
    private int width, height;                // image dimensions

    // directions to 8 neighboring pixels, clockwise: 0=North (-y), 1=NE, 2=East (+x), ... 7=NW
    private int[] dirOffset;                    // pixel offsets of neighbor pixels for direct addressing
    private final static int[] DIR_X_OFFSET = new int[] {  0,  1,  1,  1,  0, -1, -1, -1 };
    private final static int[] DIR_Y_OFFSET = new int[] { -1, -1,  0,  1,  1,  1,  0, -1 };

    // used to set bits corresponding to pixel types
    private static final byte MAXIMUM = (byte)1;            // marks local maxima (irrespective of noise tolerance)
    private static final byte LISTED = (byte)2;             // marks points currently in the list
    private static final byte PROCESSED = (byte)4;          // marks points processed previously
    private static final byte MAX_AREA = (byte)8;           // marks areas near a maximum, within the tolerance
    private static final byte EQUAL = (byte)16;             // marks contigous maximum points of equal level
    private static final byte MAX_POINT = (byte)32;         // marks a single point standing for a maximum
    private static final float SQRT2 = 1.4142135624f;

    // pixel values
    private static final int BLACK_THRESHOLD = 50;  // greatest pixel value to be considered black
    private static final int WHITE_THRESHOLD = 210;  // lowest pixel value to be considered white
    private static final int RED_THRESHOLD = 210;  // lowest R pixel value to be considered red
    private int spotMaxSize = 10;  // search dimension max
    private int spotMaxDimension = 6;  // dimension of spot at most 5 x 5 pixels
    private int spotMinDimension = 1;  // dimension of spot at least 1 x 1 pixel
    private double pixelSpotThreshold = 0.9;  // lower threshold of maxima to be considered a spot
    private int numberOfSpots = 0;

    // red area boundaries
    int start_x = 0;
    int start_y = 0;
    int end_x = width;
    int end_y = height;

    /** Method to return types supported
     * @param arg   Not used by this plugin-filter
     * @param imp   The image to be filtered
     * @return      Code describing supported formats etc.
     * (see ij.plugin.filter.PlugInFilter & ExtendedPlugInFilter)
     */
    public int setup(String arg, ImagePlus imp) {
        this.imp = imp;
        return flags;
    }

    public int showDialog(ImagePlus imp, String command, PlugInFilterRunner pfr) {
        JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        f.showSaveDialog(null);

        System.out.println(f.getCurrentDirectory());
        System.out.println(f.getSelectedFile());
        return 0;
        /*ImageProcessor ip = imp.getProcessor();
        ip.resetBinaryThreshold(); // remove any invisible threshold set by Make Binary or Convert to Mask
        thresholded = ip.getMinThreshold()!=ImageProcessor.NO_THRESHOLD;
        GenericDialog gd = new GenericDialog(command);
        int digits = (ip instanceof FloatProcessor)?2:0;
        String unit = (imp.getCalibration()!=null)?imp.getCalibration().getValueUnit():null;
        unit = (unit==null||unit.equals("Gray Value"))?":":" ("+unit+"):";
        gd.addNumericField("Noise tolerance"+unit,tolerance, digits);
        gd.addMessage("More noise implies less local bright point can be found, but they will be far more accurate.");
        gd.addNumericField("Minimum dimension of spot (pixel)",spotMinDimension, digits);
        gd.addNumericField("Maximum dimension of spot (pixel)",spotMaxDimension, digits);
        gd.addMessage("Usually 5 pixels are about 30 nm, the right size of a spot");
        gd.addCheckbox("Exclude edge maxima", excludeOnEdges);
        if (thresholded)
            gd.addCheckbox("Above lower threshold", useMinThreshold);
        gd.addPreviewCheckbox(pfr, "Preview point selection");
        gd.addMessage("    "); //space for number of maxima
        messageArea = (Label)gd.getMessage();
        gd.addDialogListener(this);
        checkboxes = gd.getCheckboxes();
        previewing = true;
        gd.showDialog();          //input by the user (or macro) happens here
        if (gd.wasCanceled())
            return DONE;
        previewing = false;
        if (!dialogItemChanged(gd, null))   //read parameters
            return DONE;
        IJ.register(this.getClass());       //protect static class variables (parameters) from garbage collection
        return flags;*/
    } // boolean showDialog

    /** Read the parameters (during preview or after showing the dialog) */
    public boolean dialogItemChanged(GenericDialog gd, AWTEvent e) {
        tolerance = gd.getNextNumber();
        if (tolerance<0) tolerance = 0;

        spotMinDimension = (int) gd.getNextNumber();
        spotMaxDimension = (int) gd.getNextNumber();
        if (spotMinDimension < 0) spotMinDimension = 0;
        if (spotMaxDimension < 0) spotMaxDimension = 0;
        if (spotMinDimension > spotMaxDimension) {
            spotMinDimension = spotMaxDimension;
        }

        excludeOnEdges = gd.getNextBoolean();
        if (thresholded)
            useMinThreshold = gd.getNextBoolean();
        else
            useMinThreshold = false;
        boolean invertedLut = imp.isInvertedLut();
        if (useMinThreshold && ((invertedLut&&!lightBackground) || (!invertedLut&&lightBackground))) {
            if (!thresholdWarningShown) return false; // if faulty input is not detected during preview, "cancel" quits
            thresholdWarningShown = true;
            useMinThreshold = false;
            ((Checkbox)(checkboxes.elementAt(1))).setState(false); //reset "Above Lower Threshold" checkbox
        }
        if (!gd.isPreviewActive())
            messageArea.setText("");        // no "nnn Maxima" message when not previewing
        return (!gd.invalidNumber());
    }

    /** Set his to the number of images to process (for the watershed progress bar only).
     *  Don't call or set nPasses to zero if no progress bar is desired. */
    public void setNPasses(int nPasses) {
        this.nPasses = nPasses;
    }

    /** The plugin is inferred from ImageJ by this method
     * @param ip The image where maxima (or minima) should be found
     */
    public void run(ImageProcessor ip) {
        JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        f.showSaveDialog(null);

        System.out.println(f.getCurrentDirectory());
        System.out.println(f.getSelectedFile());


        /*Roi roi = imp.getRoi();
        if (!roiSaved) {
            imp.saveRoi(); // save previous selection so user can restore it
            roiSaved = true;
        }
        boolean invertedLut = imp.isInvertedLut();
        double threshold = useMinThreshold?ip.getMinThreshold():ImageProcessor.NO_THRESHOLD;
        if (invertedLut) {
            threshold = ImageProcessor.NO_THRESHOLD;    // don't care about threshold when finding minima
            float[] cTable = ip.getCalibrationTable();
            ip = ip.duplicate();
            if (cTable==null) {                 // invert image for finding minima of uncalibrated images
                ip.invert();
            } else {                            // we are using getPixelValue, so the CalibrationTable must be inverted
                float[] invertedCTable = new float[cTable.length];
                for (int i=cTable.length-1; i>=0; i--)
                    invertedCTable[i] = -cTable[i];
                ip.setCalibrationTable(invertedCTable);
            }
            ip.setRoi(roi);
        }

        numberOfSpots = 0;  // reset number of spots
        findCenterRedArea(ip);
        findMaxima(ip, tolerance, threshold, excludeOnEdges, false);  // process the image*/
    }

    private void findCenterRedArea(ImageProcessor ip) {
        Rectangle roi = ip.getRoi();
        int width = roi.width;
        int height = roi.height;
        boolean hasStartCoordinateXBeenFound = false;
        int start_x = 0;  // cropping coordinates
        int end_x = width;
        int start_y = height;
        int end_y = 0;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int rawPixel = ip.getPixel(x, y);
                int red = (rawPixel >> 16) & 0xff;
                int green = (rawPixel >> 8) & 0xff;
                int blue = rawPixel & 0xff;
                boolean isApproximatelyRed = (red > RED_THRESHOLD) && (green < BLACK_THRESHOLD) && (blue < BLACK_THRESHOLD);
                if (isApproximatelyRed) {
                    // update x coordinates
                    if (hasStartCoordinateXBeenFound) {
                        // already found start x coordinate
                        end_x = x;
                    } else {
                        // found start x coordinate
                        start_x = x;
                        hasStartCoordinateXBeenFound = true;
                    }

                    // update y coordinates
                    if (start_y > y) {
                        start_y = y;
                    }

                    if (end_y < y) {
                        end_y = y;
                    }
                }
            }
        }
        // correct misleading values
        if (start_x > width * 0.66) {
            this.start_x = 0;
        } else {
            this.start_x = start_x;
        }
        if (start_y > height * 0.5) {
            this.start_y = 0;
        } else {
            this.start_y = start_y;
        }
        if (end_x < width * 0.66) {
            this.end_x = width;
        } else {
            this.end_x = end_x;
        }
        if (end_y < height * 0.5) {
            this.end_y = height;
        } else {
            this.end_y = end_y;
        }
    }

    /** Here the processing is done: Find the maxima of an image (does not find minima).
     *
     * LIMITATIONS:          With outputType=SEGMENTED (watershed segmentation), some segmentation lines
     *                       may be improperly placed if local maxima are suppressed by the tolerance.
     *
     * @param ip             The input image
     * @param tolerance      Height tolerance: maxima are accepted only if protruding more than this value
     *                       from the ridge to a higher maximum
     * @param threshold      minimum height of a maximum (uncalibrated); for no minimum height set it to
     *                       ImageProcessor.NO_THRESHOLD
     * @param excludeOnEdges Whether to exclude edge maxima
     * @param isEDM          Whether the image is a float Euclidian Distance Map.
     * @return               A new byteProcessor with a normal (uninverted) LUT where the marked points
     *                       are set to 255 (Background 0). Pixels outside of the roi of the input ip are not set.
     *                       Returns null if outputType does not require an output or if cancelled by escape
     */
    private void findMaxima(ImageProcessor ip, double tolerance, double threshold, boolean excludeOnEdges, boolean isEDM) {
        if (dirOffset == null) makeDirectionOffsets(ip);
        Rectangle roi = ip.getRoi();
        if (threshold!=ImageProcessor.NO_THRESHOLD && ip.getCalibrationTable()!=null &&
                threshold>0 && threshold<ip.getCalibrationTable().length)
            threshold = ip.getCalibrationTable()[(int)threshold];   //convert threshold to calibrated
        ByteProcessor typeP = new ByteProcessor(width, height);     //will be a notepad for pixel types
        float globalMin = Float.MAX_VALUE;
        float globalMax = -Float.MAX_VALUE;
        for (int y=roi.y; y<roi.y+roi.height; y++) {         //find local minimum/maximum now
            for (int x=roi.x; x<roi.x+roi.width; x++) {      //ImageStatistics won't work if we have no ImagePlus
                float v = ip.getPixelValue(x, y);
                if (globalMin>v) globalMin = v;
                if (globalMax<v) globalMax = v;
            }
        }
        if (threshold !=ImageProcessor.NO_THRESHOLD)
            threshold -= (globalMax-globalMin)*1e-6;//avoid rounding errors
        //for segmentation, exclusion of edge maxima cannot be done now but has to be done after segmentation:
        boolean excludeEdgesNow = excludeOnEdges;

        if (Thread.currentThread().isInterrupted()) {
            return;
        }

        IJ.showStatus("Getting sorted maxima...");
        long[] maxPoints = getSortedMaxPoints(ip, typeP, excludeEdgesNow, isEDM, globalMin, globalMax, threshold);
        if (Thread.currentThread().isInterrupted()) {
            return;
        }

        IJ.showStatus("Analyzing maxima...");
        float maxSortingError = 0;
        if (ip instanceof FloatProcessor) {
            maxSortingError = 1.1f * (isEDM ? SQRT2/2f : (globalMax-globalMin)/2e9f);  //sorted sequence may be inaccurate by this value
        }

        Vector spots = getLocalMaxima(ip, typeP, maxPoints, excludeEdgesNow, isEDM, tolerance, maxSortingError);
        markPoints(spots);
    }

    /** Find all local maxima (irrespective whether they finally qualify as maxima or not)
     * @param ip    The image to be analyzed
     * @param typeP A byte image, same size as ip, where the maximum points are marked as MAXIMUM
     *              (do not use it as output: for rois, the points are shifted w.r.t. the input image)
     * @param excludeEdgesNow Whether to exclude edge pixels
     * @param isEDM     Whether ip is a float Euclidian distance map
     * @param globalMin The minimum value of the image or roi
     * @param threshold The threshold (calibrated) below which no pixels are processed. Ignored if ImageProcessor.NO_THRESHOLD
     * @return          Maxima sorted by value. In each array element (long, i.e., 64-bit integer), the value
     *                  is encoded in the upper 32 bits and the pixel offset in the lower 32 bit
     * Note: Do not use the positions of the points marked as MAXIMUM in typeP, they are invalid for images with a roi.
     */
    private long[] getSortedMaxPoints(ImageProcessor ip, ByteProcessor typeP, boolean excludeEdgesNow, boolean isEDM, float globalMin, float globalMax, double threshold) {
        Rectangle roi = ip.getRoi();
        byte[] types =  (byte[])typeP.getPixels();
        int nMax = 0;  //counts local maxima
        boolean checkThreshold = threshold!=ImageProcessor.NO_THRESHOLD;
        Thread thread = Thread.currentThread();
        for (int y=roi.y; y<roi.y+roi.height; y++) {         // find local maxima now
            if (y%50==0 && thread.isInterrupted()) return null;
            for (int x=roi.x, i=x+y*width; x<roi.x+roi.width; x++, i++) {      // for better performance with rois, restrict search to roi
                float v = ip.getPixelValue(x,y);
                float vTrue = isEDM ? trueEdmHeight(x,y,ip) : v;  // for EDMs, use interpolated ridge height
                if (v==globalMin) continue;
                if (excludeEdgesNow && (x==0 || x==width-1 || y==0 || y==height-1)) continue;
                if (checkThreshold && v<threshold) continue;
                boolean isMax = true;
                /* check wheter we have a local maximum.
                 Note: For an EDM, we need all maxima: those of the EDM-corrected values
                 (needed by findMaxima) and those of the raw values (needed by cleanupMaxima) */
                boolean isInner = (y!=0 && y!=height-1) && (x!=0 && x!=width-1); //not necessary, but faster than isWithin
                for (int d=0; d<8; d++) {                         // compare with the 8 neighbor pixels
                    if (isInner || isWithin(x, y, d)) {
                        float vNeighbor = ip.getPixelValue(x+DIR_X_OFFSET[d], y+DIR_Y_OFFSET[d]);
                        float vNeighborTrue = isEDM ? trueEdmHeight(x+DIR_X_OFFSET[d], y+DIR_Y_OFFSET[d], ip) : vNeighbor;
                        if (vNeighbor > v && vNeighborTrue > vTrue) {
                            isMax = false;
                            break;
                        }
                    }
                }
                if (isMax) {
                    types[i] = MAXIMUM;
                    nMax++;
                }
            } // for x
        } // for y
        if (thread.isInterrupted()) return null;
        //long t1 = System.currentTimeMillis();IJ.log("markMax:"+(t1-t0));

        float vFactor = (float)(2e9/(globalMax-globalMin)); //for converting float values into a 32-bit int
        long[] maxPoints = new long[nMax];                  //value (int) is in the upper 32 bit, pixel offset in the lower
        int iMax = 0;
        for (int y=roi.y; y<roi.y+roi.height; y++)           //enter all maxima into an array
            for (int x=roi.x, p=x+y*width; x<roi.x+roi.width; x++, p++)
                if (types[p]==MAXIMUM) {
                    float fValue = isEDM?trueEdmHeight(x,y,ip):ip.getPixelValue(x,y);
                    int iValue = (int)((fValue-globalMin)*vFactor); //32-bit int, linear function of float value
                    maxPoints[iMax++] = (long)iValue<<32|p;
                }
        if (thread.isInterrupted()) return null;
        Arrays.sort(maxPoints);                                 //sort the maxima by value
        return maxPoints;
    }

    /** Check all maxima in list maxPoints, mark type of the points in typeP
     * @param ip             the image to be analyzed
     * @param typeP          8-bit image, here the point types are marked by type: MAX_POINT, etc.
     * @param maxPoints      input: a list of all local maxima, sorted by height. Lower 32 bits are pixel offset
     * @param excludeEdgesNow whether to avoid edge maxima
     * @param isEDM          whether ip is a (float) Euclidian distance map
     * @param tolerance      minimum pixel value difference for two separate maxima
     * @param maxSortingError sorting may be inaccurate, sequence may be reversed for maxima having values
     *                       not deviating from each other by more than this (this could be a result of
     *                       precision loss when sorting ints instead of floats, or because sorting does not
     *                       take the height correction in 'trueEdmHeight' into account
     */
    private Vector getLocalMaxima(ImageProcessor ip, ByteProcessor typeP, long[] maxPoints, boolean excludeEdgesNow, boolean isEDM, double tolerance, float maxSortingError) {
        byte[] types =  (byte[])typeP.getPixels();
        float[] edmPixels = isEDM ? (float[])ip.getPixels() : null;
        int nMax = maxPoints.length;
        int [] pList = new int[width*height];       //here we enter points starting from a maximum
        Vector xyVector = new Vector();
        Roi roi = imp.getRoi();
        for (int iMax=nMax-1; iMax>=0; iMax--) {    //process all maxima now, starting from the highest
            if (iMax%100 == 0 && Thread.currentThread().isInterrupted()) return null;
            int offset0 = (int)maxPoints[iMax];     //type cast gets 32 lower bits, where pixel index is encoded
            if ((types[offset0]&PROCESSED)!=0)      //this maximum has been reached from another one, skip it
                continue;
            // we create a list of connected points and start the list at the current maximum
            int x0 = offset0 % width;
            int y0 = offset0 / width;
            float v0 = isEDM?trueEdmHeight(x0,y0,ip):ip.getPixelValue(x0,y0);
            boolean sortingError;
            do {                                    //repeat if we have encountered a sortingError
                pList[0] = offset0;
                types[offset0] |= (EQUAL|LISTED);   //mark first point as equal height (to itself) and listed
                int listLen = 1;                    //number of elements in the list
                int listI = 0;                      //index of current element in the list
                boolean isEdgeMaximum = (x0==0 || x0==width-1 || y0==0 || y0==height-1);
                sortingError = false;       //if sorting was inaccurate: a higher maximum was not handled so far
                boolean maxPossible = true;         //it may be a true maximum
                double xEqual = x0;                 //for creating a single point: determine average over the
                double yEqual = y0;                 //  coordinates of contiguous equal-height points
                int nEqual = 1;                     //counts xEqual/yEqual points that we use for averaging
                do {                                //while neigbor list is not fully processed (to listLen)
                    int offset = pList[listI];
                    int x = offset % width;
                    int y = offset / width;
                    boolean isInner = (y!=0 && y!=height-1) && (x!=0 && x!=width-1); //not necessary, but faster than isWithin
                    for (int d=0; d<8; d++) {       //analyze all neighbors (in 8 directions) at the same level
                        int offset2 = offset+dirOffset[d];
                        if ((isInner || isWithin(x, y, d)) && (types[offset2]&LISTED)==0) {
                            if (isEDM && edmPixels[offset2]<=0) continue;   //ignore the background (non-particles)
                            if ((types[offset2]&PROCESSED)!=0) {
                                maxPossible = false; //we have reached a point processed previously, thus it is no maximum now
                                break;
                            }
                            int x2 = x+DIR_X_OFFSET[d];
                            int y2 = y+DIR_Y_OFFSET[d];
                            float v2 = isEDM ? trueEdmHeight(x2, y2, ip) : ip.getPixelValue(x2, y2);
                            if (v2 > v0 + maxSortingError) {
                                maxPossible = false;    //we have reached a higher point, thus it is no maximum
                                break;
                            } else if (v2 >= v0-(float)tolerance) {
                                if (v2 > v0) {          //maybe this point should have been treated earlier
                                    sortingError = true;
                                    offset0 = offset2;
                                    v0 = v2;
                                    x0 = x2;
                                    y0 = y2;
                                }
                                pList[listLen] = offset2;
                                listLen++;              //we have found a new point within the tolerance
                                types[offset2] |= LISTED;
                                if (x2==0 || x2==width-1 || y2==0 || y2==height-1) {
                                    isEdgeMaximum = true;
                                    if (excludeEdgesNow) {
                                        maxPossible = false;
                                        break;          //we have an edge maximum;
                                    }
                                }
                                if (v2==v0) {           //prepare finding center of equal points (in case single point needed)
                                    types[offset2] |= EQUAL;
                                    xEqual += x2;
                                    yEqual += y2;
                                    nEqual ++;
                                }
                            }
                        } // if isWithin & not LISTED
                    } // for directions d
                    listI++;
                } while (listI < listLen);

                if (sortingError)  {				  //if x0,y0 was not the true maximum but we have reached a higher one
                    for (listI=0; listI<listLen; listI++)
                        types[pList[listI]] = 0;	//reset all points encountered, then retry
                } else {
                    int resetMask = ~(maxPossible?LISTED:(LISTED|EQUAL));
                    xEqual /= nEqual;
                    yEqual /= nEqual;
                    double minDist2 = 1e20;
                    int nearestI = 0;
                    for (listI=0; listI<listLen; listI++) {
                        int offset = pList[listI];
                        int x = offset % width;
                        int y = offset / width;
                        types[offset] &= resetMask;		// reset attributes no longer needed
                        types[offset] |= PROCESSED;		// mark as processed
                        if (maxPossible) {
                            types[offset] |= MAX_AREA;
                            if ((types[offset]&EQUAL)!=0) {
                                double dist2 = (xEqual-x)*(xEqual-x) + (yEqual-y)*(yEqual-y);
                                if (dist2 < minDist2) {
                                    minDist2 = dist2;	//this could be the best "single maximum" point
                                    nearestI = listI;
                                }
                            }
                        }
                    } // for listI
                    if (maxPossible) {
                        int offset = pList[nearestI];
                        types[offset] |= MAX_POINT;
                        if (!(excludeOnEdges && isEdgeMaximum)) {
                            int x = offset % width;
                            int y = offset / width;
                            if (roi == null || roi.contains(x, y)) {
                                boolean isWithinRedEdges = (x >= start_x && x <= end_x) && (y >= start_y && y <= end_y);
                                if(isWithinRedEdges) {
                                    Vector neighbourPixels = findSpot(ip, x, y);
                                    xyVector.addAll(neighbourPixels);
                                }
                            }
                        }
                    }
                }
            } while (sortingError);  // redo if we have encountered a higher maximum: handle it now.
        }

        return xyVector;
    }

    private Vector findSpot(ImageProcessor ip, int xCoordinate, int yCoordinate) {
        int pixelColor = ip.getPixel(xCoordinate, yCoordinate);
        int red = (pixelColor >> 16) & 0xff;
        int green = (pixelColor >> 8) & 0xff;
        int blue = pixelColor & 0xff;
        int grayScaled = (red + green + blue) / 3;
        boolean isBlack = grayScaled < BLACK_THRESHOLD;  // is approximately black
        boolean isWhite = grayScaled > WHITE_THRESHOLD;  // is approximately white
        if (!isBlack && !isWhite && !(red > RED_THRESHOLD && green < BLACK_THRESHOLD && blue < BLACK_THRESHOLD)) {
            Vector spotArea = getSpot(ip, xCoordinate, yCoordinate);
            int[] size_spot = (int[]) spotArea.elementAt(spotArea.size() - 1);
            int spot_width = size_spot[0];
            int spot_height = size_spot[1];
            boolean isWidthWithinRange = spot_width >= spotMinDimension && spot_width <= spotMaxDimension;
            boolean isHeigthWithinRange = spot_height >= spotMinDimension && spot_height <= spotMaxDimension;
            boolean isAreaWithinRange = spot_width * spot_height >= spotMinDimension * spotMinDimension && spot_width * spot_height <= spotMaxDimension * spotMaxDimension;
            if (isWidthWithinRange || isHeigthWithinRange && isAreaWithinRange) {  // found a proper spot
                spotArea.remove(spotArea.size() - 1);  // remove size
                numberOfSpots++;
                return spotArea;
            }
        }
        return new Vector();
    }

    private Vector getSpot(ImageProcessor ip, int xCoordinate, int yCoordinate) {
        int xyColor = ip.getPixel(xCoordinate, yCoordinate);
        xyColor = ((xyColor >> 16) & 0xff) + ((xyColor >> 8) & 0xff) + (xyColor & 0xff);
        xyColor /= 3;
        int pixelColor, red, green, blue, min_x = width, min_y = height, max_x = 0, max_y = 0;
        Vector spots = new Vector();
        for (int x = xCoordinate - spotMaxSize / 2 - 1; x < xCoordinate + spotMaxSize / 2 + 1; x++) {
            for (int y = yCoordinate - spotMaxSize / 2 - 1 ; y < yCoordinate + spotMaxSize / 2 + 1; y++) {
                pixelColor = ip.getPixel(x, y);
                red = (pixelColor >> 16) & 0xff;
                green = (pixelColor >> 8) & 0xff;
                blue = pixelColor & 0xff;
                pixelColor = (red + green + blue) / 3;
                if (pixelColor >= pixelSpotThreshold * xyColor) {
                    spots.addElement(new int[]{x, y});

                    // update boundaires
                    if(x < min_x) {
                        min_x = x;
                    }

                    if(x > max_x) {
                        max_x = x;
                    }

                    if(y < min_y) {
                        min_y = y;
                    }

                    if(y > max_y) {
                        max_y = y;
                    }
                }
            }
        }

        int spot_width = max_x - min_x;
        int spot_height = max_y - min_y;

        // correct values
        if (spot_width == width) {
            spot_width = spotMaxDimension;
        }

        if (spot_height == height) {
            spot_height = spotMaxDimension;
        }

        spots.add(new int[]{spot_width, spot_height});  // add size of spot
        return spots;
    }

    private void markPoints(Vector xyVector) {
        if (Thread.currentThread().isInterrupted()) return;
        int npoints = xyVector.size();
        if (npoints > 0) {
            int[] firstPoint = (int[]) xyVector.elementAt(0);
            int[] lastPoint = (int[]) xyVector.elementAt(npoints - 1);
            int start_x = firstPoint[0];
            int start_y = firstPoint[1];
            int width = lastPoint[0] - start_x;
            int height = lastPoint[1] - start_y;
            // preview as circles
            // EllipseRoi previewRoi = new EllipseRoi(start_x, start_y, width, height, 1.0);

            // preview as points target
            int[] xpoints = new int[npoints];
            int[] ypoints = new int[npoints];
            for (int i=0; i<npoints; i++) {
                int[] xy = (int[]) xyVector.elementAt(i);
                xpoints[i] = xy[0];
                ypoints[i] = xy[1];
            }
            PointRoi previewRoi = new PointRoi(xpoints, ypoints, npoints);
            imp.setRoi(previewRoi);
        }

        if (previewing) messageArea.setText(Integer.toString(numberOfSpots) + " Spots");
    }

    /** Get estimated "true" height of a maximum or saddle point of a Euclidian Distance Map.
     * This is needed since the point sampled is not necessarily at the highest position.
     * For simplicity, we don't care about the Sqrt(5) distance here although this would be more accurate
     * @param x     x-position of the point
     * @param y     y-position of the point
     * @param ip    the EDM (FloatProcessor)
     * @return      estimated height
     */
    private float trueEdmHeight(int x, int y, ImageProcessor ip) {
        int xmax = width - 1;
        int ymax = ip.getHeight() - 1;
        float[] pixels = (float[])ip.getPixels();
        int offset = x + y*width;
        float v =  pixels[offset];
        if (x==0 || y==0 || x==xmax || y==ymax || v==0) {
            return v;                               //don't recalculate for edge pixels or background
        } else {
            float trueH = v + 0.5f*SQRT2;           //true height can never by higher than this
            boolean ridgeOrMax = false;
            for (int d=0; d<4; d++) {               //for all directions halfway around:
                int d2 = (d+4)%8;                   //get the opposite direction and neighbors
                float v1 = pixels[offset+dirOffset[d]];
                float v2 = pixels[offset+dirOffset[d2]];
                float h;
                if (v>=v1 && v>=v2) {
                    ridgeOrMax = true;
                    h = (v1 + v2)/2;
                } else {
                    h = Math.min(v1, v2);
                }
                h += (d%2==0) ? 1 : SQRT2;          //in diagonal directions, distance is sqrt2
                if (trueH > h) trueH = h;
            }
            if (!ridgeOrMax) trueH = v;
            return trueH;
        }
    }

    /** create an array of offsets within a pixel array for directions in clockwise order:
     * 0=(x,y-1), 1=(x+1,y-1), ... 7=(x-1,y)
     * Also creates further class variables:
     * width, height, and the following three values needed for storing coordinates in single ints for watershed:
     * intEncodeXMask, intEncodeYMask and intEncodeShift.
     * E.g., for width between 129 and 256, xMask=0xff and yMask = 0xffffff00 are bitwise masks
     * for x and y, respectively, and shift=8 is the bit shift to get y from the y-masked value
     * Returns as class variables: the arrays of the offsets to the 8 neighboring pixels
     * and the array maskAndShift for watershed
     */
    private void makeDirectionOffsets(ImageProcessor ip) {
        width = ip.getWidth();
        height = ip.getHeight();
        int shift = 0, mult=1;
        do {
            shift++; mult*=2;
        }
        while (mult < width);
        //IJ.log("masks (hex):"+Integer.toHexString(xMask)+","+Integer.toHexString(xMask)+"; shift="+shift);
        dirOffset  = new int[] {-width, -width+1, +1, +width+1, +width, +width-1,   -1, -width-1 };
        //dirOffset is created last, so check for it being null before makeDirectionOffsets
        //(in case we have multiple threads using the same MaximumFinder)
    }

    /** returns whether the neighbor in a given direction is within the image
     * NOTE: it is assumed that the pixel x,y itself is within the image!
     * Uses class variables width, height: dimensions of the image
     * @param x         x-coordinate of the pixel that has a neighbor in the given direction
     * @param y         y-coordinate of the pixel that has a neighbor in the given direction
     * @param direction the direction from the pixel towards the neighbor (see makeDirectionOffsets)
     * @return          true if the neighbor is within the image (provided that x, y is within)
     */
    private boolean isWithin(int x, int y, int direction) {
        int xmax = width - 1;
        int ymax = height -1;
        switch(direction) {
            case 0:
                return (y>0);
            case 1:
                return (x<xmax && y>0);
            case 2:
                return (x<xmax);
            case 3:
                return (x<xmax && y<ymax);
            case 4:
                return (y<ymax);
            case 5:
                return (x>0 && y<ymax);
            case 6:
                return (x>0);
            case 7:
                return (x>0 && y>0);
        }
        return false;   //to make the compiler happy :-)
    }
}
