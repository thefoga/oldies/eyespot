/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin.tool;

import ij.ImagePlus;
import ij.gui.Toolbar;
import ij.macro.Program;
import ij.plugin.PlugIn;

import java.awt.event.MouseEvent;

public abstract class PlugInTool implements PlugIn {

	public void run(String arg) {
		Toolbar.addPlugInTool(this);
	}
	
	public void mousePressed(ImagePlus imp, MouseEvent e) {e.consume();}

	public void mouseReleased(ImagePlus imp, MouseEvent e) {e.consume();}

	public void mouseClicked(ImagePlus imp, MouseEvent e) {e.consume();}

	public void mouseDragged(ImagePlus imp, MouseEvent e) {e.consume();}
	
	public void mouseMoved(ImagePlus imp, MouseEvent e) { }
	
	public void mouseEntered(ImagePlus imp, MouseEvent e) {e.consume();}

	public void mouseExited(ImagePlus imp, MouseEvent e) {e.consume();}
	
	public void showPopupMenu(MouseEvent e, Toolbar tb) { }

	/** Return the tool name. */
	public String getToolName() {
		return getClass().getName().replace('_', ' ');
	}
	
	/** Return the string encoding of the tool icon. See
		http://rsb.info.nih.gov/ij/developer/macro/macros.html#icons
		The default icon is the first letter of the tool name.
	*/
	public String getToolIcon() {
		String letter = getToolName();
		if (letter!=null && letter.length()>0)
			letter = letter.substring(0,1);
		else
			letter = "P";
		return "C037T5f16"+letter;
	}
	
	public void showOptionsDialog() {
	}

	/** These methods are overridden by MacroToolRunner. */
	public void runMacroTool(String name) { }
	public void runMenuTool(String name, String command) { }
	public Program getMacroProgram() {return null;}

}
