/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin.filter;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.DialogListener;
import ij.gui.GenericDialog;
import ij.process.ImageProcessor;

import java.awt.*;


/** This plugin implements the Image/Translate command. */
public class Translator implements ExtendedPlugInFilter, DialogListener {
	private int flags = DOES_ALL|PARALLELIZE_STACKS;
	private static double xOffset = 15;
	private static double yOffset = 15;
	private ImagePlus imp;
	private GenericDialog gd;
	private PlugInFilterRunner pfr;
	private static int interpolationMethod = ImageProcessor.NONE;
	private String[] methods = ImageProcessor.getInterpolationMethods();

	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return flags;
	}

	public void run(ImageProcessor ip) {
		ip.setInterpolationMethod(interpolationMethod);
		ip.translate(xOffset, yOffset);
	}

	public int showDialog(ImagePlus imp, String command, PlugInFilterRunner pfr) {
		this.pfr = pfr;
		int digits = xOffset==(int)xOffset&&yOffset==(int)yOffset?1:3;
		if (IJ.isMacro())
			interpolationMethod = ImageProcessor.NONE;
		gd = new GenericDialog("Translate");
		gd.addNumericField("X offset (pixels): ", xOffset, digits, 8, "");
		gd.addNumericField("Y offset (pixels): ", yOffset, digits, 8, "");
		gd.addChoice("Interpolation:", methods, methods[interpolationMethod]);
		gd.addPreviewCheckbox(pfr);
		gd.addDialogListener(this);
		gd.showDialog();
		if (gd.wasCanceled())
			return DONE;
		return IJ.setupDialog(imp, flags);
	}
	
	public boolean dialogItemChanged(GenericDialog gd, AWTEvent e) {
		xOffset = gd.getNextNumber();
		yOffset = gd.getNextNumber();
		interpolationMethod = gd.getNextChoiceIndex();
		if (gd.invalidNumber()) {
			if (gd.wasOKed()) IJ.error("Offset is invalid.");
			return false;
		}
		return true;
	}

	public void setNPasses(int nPasses) {
	}

}

