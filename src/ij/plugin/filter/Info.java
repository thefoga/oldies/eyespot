/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.plugin.filter;

import ij.ImagePlus;
import ij.plugin.ImageInfo;
import ij.process.ImageProcessor;

/**
* @deprecated
* replaced by ij.plugin.ImageInfo
*/
public class Info implements PlugInFilter {
    private ImagePlus imp;

	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return DOES_ALL+NO_CHANGES;
	}

	public void run(ImageProcessor ip) {
	}
	
	public String getImageInfo(ImagePlus imp, ImageProcessor ip) {
		ImageInfo info = new ImageInfo();
		return info.getImageInfo(imp);
	}

}
