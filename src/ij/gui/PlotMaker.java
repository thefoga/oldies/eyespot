/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.gui;
import ij.ImagePlus;

/** Plugins that generate "Live" profile plots (Profiler and ZAxisProfiler)
	displayed in PlotWindows implement this interface. */
public interface PlotMaker {

   /** Returns a profile plot. */
   public Plot getPlot();
   
   /** Returns the ImagePlus used to generate the profile plots. */
   public ImagePlus getSourceImage();

}

