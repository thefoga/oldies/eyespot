/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.gui;
import ij.ImagePlus;
	
	/** Plugins that implement this interface are notified when
		an ROI is created, modified or deleted. The 
		Plugins/Utilities/Monitor Events command uses this interface.
	*/
	public interface RoiListener {
		public static final int CREATED = 1;
		public static final int MOVED = 2;
		public static final int MODIFIED = 3;
		public static final int EXTENDED = 4;
		public static final int COMPLETED = 5;
		public static final int DELETED = 6;

	public void roiModified(ImagePlus imp, int id);

}
