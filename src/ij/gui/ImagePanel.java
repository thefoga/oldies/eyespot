/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.gui;

import ij.ImagePlus;

import java.awt.*;

/** This class is used by GenericDialog to add images to dialogs. */
public class ImagePanel extends Panel {
	private ImagePlus img;
	private int width, height;
	 
	ImagePanel(ImagePlus img) {
		this.img = img;
		width = img.getWidth();
		height = img.getHeight();
	}

	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}

	public Dimension getMinimumSize() {
		return new Dimension(width, height);
	}

	public void paint(Graphics g) {
		g.drawImage(img.getProcessor().createImage(), 0, 0, null);
	}

}
