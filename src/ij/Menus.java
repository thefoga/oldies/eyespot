/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij;

import ij.gui.ImageWindow;
import ij.plugin.MacroInstaller;
import ij.util.StringSorter;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.*;

/**
This class installs and updates ImageJ's menus. Note that menu labels,
even in submenus, must be unique. This is because ImageJ uses a single
hash table for all menu labels. If you look closely, you will see that
File->Import->Text Image... and File->Save As->Text Image... do not use
the same label. One of the labels has an extra space.

@see ImageJ
*/

public class Menus {

	public static final char PLUGINS_MENU = 'p';
	public static final char IMPORT_MENU = 'i';
	public static final char SAVE_AS_MENU = 's';
	public static final char SHORTCUTS_MENU = 'h'; // 'h'=hotkey
	public static final char ABOUT_MENU = 'a';
	public static final char FILTERS_MENU = 'f';
	public static final char TOOLS_MENU = 't';
	public static final char UTILITIES_MENU = 'u';
		
	public static final int WINDOW_MENU_ITEMS = 6; // fixed items at top of Window menu
	
	public static final int NORMAL_RETURN = 0;
	public static final int COMMAND_IN_USE = -1;
	public static final int INVALID_SHORTCUT = -2;
	public static final int SHORTCUT_IN_USE = -3;
	public static final int COMMAND_NOT_FOUND = -5;
	
	public static final int MAX_OPEN_RECENT_ITEMS = 15;

	private static Menus instance;
	private static MenuBar mbar;
	private static PopupMenu popup;

	private static ImageJ ij;
	private static Applet applet;
	private static String ImageJPath, pluginsPath, macrosPath;
	private static Properties menus;
	private static Menu pluginsMenu, saveAsMenu;
	static Menu window, openRecentMenu;
	private static Hashtable pluginsTable;
	
	private static int nPlugins, nMacros;
	private static Hashtable shortcuts;
	private static Hashtable macroShortcuts;
	private static Vector pluginsPrefs; // commands saved in IJ_Prefs
	static int windowMenuItems2; // non-image windows listed in Window menu + separator
	private String error;
	private String jarError;
	private String pluginError;
    private boolean isJarErrorHeading;
	private static boolean installingJars, duplicateCommand;
	private static Vector jarFiles;  // JAR files in plugins folder with "_" in their name
	private Map menuEntry2jarFile = new HashMap();
	private static Vector macroFiles;  // Macros and scripts in the plugins folder
	private static int userPluginsIndex; // First user plugin or submenu in Plugins menu
	private static boolean addSorted;
	private static int defaultFontSize = IJ.isWindows()?15:0;
	private static int fontSize = Prefs.getInt(Prefs.MENU_SIZE, defaultFontSize);
	private static Font menuFont;

	static boolean jnlp; // true when using Java WebStart
	public static int setMenuBarCount;
		
	Menus(ImageJ ijInstance, Applet appletInstance) {
		ij = ijInstance;
		String title = ij!=null?ij.getTitle():null;
		applet = appletInstance;
		instance = this;
	}

	String addMenuBar() {
		nPlugins = nMacros = userPluginsIndex = 0;
		addSorted = installingJars = duplicateCommand = false;
		error = null;
		mbar = null;
		menus = new Properties();
		pluginsTable = new Hashtable();
		shortcuts = new Hashtable();
		pluginsPrefs = new Vector();
		macroShortcuts = null;
		setupPluginsAndMacrosPaths();
		Menu file = getMenu("File");
		Menu newMenu = getMenu("File>New", true);
		addPlugInItem(file, "Open...", "ij.plugin.Commands(\"open\")", KeyEvent.VK_O, false);
		addPlugInItem(file, "Open Next", "ij.plugin.NextImageOpener", KeyEvent.VK_O, true);
		addOpenRecentSubMenu(file);
		file.addSeparator();
		addPlugInItem(file, "Close", "ij.plugin.Commands(\"close\")", KeyEvent.VK_W, false);
		addPlugInItem(file, "Close All", "ij.plugin.Commands(\"close-all\")", KeyEvent.VK_W, true);
		addPlugInItem(file, "Save", "ij.plugin.Commands(\"save\")", KeyEvent.VK_S, false);
		saveAsMenu = getMenu("File>Save As", true);
		addPlugInItem(file, "Print...", "ij.plugin.filter.Printer(\"print\")", KeyEvent.VK_P, false);
		
		Menu edit = getMenu("Edit");
		addPlugInItem(edit, "Undo", "ij.plugin.Commands(\"undo\")", KeyEvent.VK_Z, false);
		edit.addSeparator();
		addPlugInItem(edit, "Cut", "ij.plugin.Clipboard(\"cut\")", KeyEvent.VK_X, false);
		addPlugInItem(edit, "Copy", "ij.plugin.Clipboard(\"copy\")", KeyEvent.VK_C, false);
		addPlugInItem(edit, "Paste", "ij.plugin.Clipboard(\"paste\")", KeyEvent.VK_V, false);
		edit.addSeparator();
		addPlugInItem(edit, "Clear", "ij.plugin.filter.Filler(\"clear\")", 0, false);
		addPlugInItem(edit, "Fill", "ij.plugin.filter.Filler(\"fill\")", KeyEvent.VK_F, false);
		addPlugInItem(edit, "Draw", "ij.plugin.filter.Filler(\"draw\")", KeyEvent.VK_D, false);
		addPlugInItem(edit, "Invert", "ij.plugin.filter.Filters(\"invert\")", KeyEvent.VK_I, true);
		edit.addSeparator();
		getMenu("Edit>Selection", true);
		Menu optionsMenu = getMenu("Edit>Options", true);
		
		Menu image = getMenu("Image");
		getMenu("Image>Info", true);
		addPlugInItem(image, "Show Info...", "ij.plugin.ImageInfo", KeyEvent.VK_I, false);
		addPlugInItem(image, "Properties...", "ij.plugin.filter.ImageProperties", KeyEvent.VK_P, true);
		getMenu("Image>Color", true);
		image.addSeparator();
		addPlugInItem(image, "Crop", "ij.plugin.Resizer(\"crop\")", KeyEvent.VK_X, true);
		addPlugInItem(image, "Duplicate...", "ij.plugin.Duplicator", KeyEvent.VK_D, true);
		addPlugInItem(image, "Rename...", "ij.plugin.SimpleCommands(\"rename\")", 0, false);
		addPlugInItem(image, "Scale...", "ij.plugin.Scaler", KeyEvent.VK_E, false);
		getMenu("Image>Transform", true);
		getMenu("Image>Zoom", true);
		getMenu("Image>Overlay", true);

		Menu process = getMenu("Process");
		addPlugInItem(process, "Spot...", "ij.plugin.eyespot.Spot", 0, false);
		// addPlugInItem(process, "Spot all in directory...", "ij.plugin.eyespot.SpotFolder", 0, false);
		addPlugInItem(process, "Smooth", "ij.plugin.filter.Filters(\"smooth\")", KeyEvent.VK_S, true);
		addPlugInItem(process, "Sharpen", "ij.plugin.filter.Filters(\"sharpen\")", 0, false);
		addPlugInItem(process, "Find Edges", "ij.plugin.filter.Filters(\"edge\")", 0, false);
		addPlugInItem(process, "Enhance Contrast...", "ij.plugin.ContrastEnhancer", 0, false);
		getMenu("Process>Binary", true);
		getMenu("Process>Math", true);
		getMenu("Process>FFT", true);
		Menu filtersMenu = getMenu("Process>Filters", true);

		Menu window = getMenu("Window");
		addPlugInItem(window, "Show All", "ij.plugin.WindowOrganizer(\"show\")", KeyEvent.VK_CLOSE_BRACKET, false);
		String key = IJ.isWindows()?"enter":"return";
		addPlugInItem(window, "Main Window ["+key+"]", "ij.plugin.WindowOrganizer(\"imagej\")", 0, false);
		addPlugInItem(window, "Put Behind [tab]", "ij.plugin.Commands(\"tab\")", 0, false);
		addPlugInItem(window, "Cascade", "ij.plugin.WindowOrganizer(\"cascade\")", 0, false);
		addPlugInItem(window, "Tile", "ij.plugin.WindowOrganizer(\"tile\")", 0, false);

		Menu help = getMenu("Help");
		addPlugInItem(help, "About Eyespot", "ij.plugin.AboutBox", 0, false);

		// make	sure "Quit" is the last item in the File menu
		file.addSeparator();
		addPlugInItem(file, "Quit", "ij.plugin.Commands(\"quit\")", 0, false);

		if (fontSize!=0)
			mbar.setFont(getFont());
		if (ij!=null) {
			ij.setMenuBar(mbar);
			Menus.setMenuBarCount++;
		}
		
		if (pluginError!=null)
			error = error!=null?error+="\n"+pluginError:pluginError;
		if (jarError!=null)
			error = error!=null?error+="\n"+jarError:jarError;
		return error;
	}
	
	public static Menu getExamplesMenu(ActionListener listener) {
		Menu menu = new Menu("Examples");
		Menu submenu = new Menu("Macro");
		addExample(submenu, "Sphere", "Sphere.ijm");
		addExample(submenu, "Dialog Box", "Dialog_Box.ijm");
		addExample(submenu, "Example Plot", "Example_Plot.ijm");
		addExample(submenu, "Semi-log Plot", "Semi-log_Plot.ijm");
		addExample(submenu, "Arrow Plot", "Arrow_Plot.ijm");
		addExample(submenu, "Process Folder", "Batch_Process_Folder.ijm");
		addExample(submenu, "OpenDialog Demo", "OpenDialog_Demo.ijm");
		addExample(submenu, "Sine/Cosine Table", "Sine_Cosine_Table.ijm");
		addExample(submenu, "Non-numeric Table", "Non-numeric_Table.ijm");
		addExample(submenu, "Overlay", "Overlay.ijm");
		addExample(submenu, "Stack Overlay", "Stack_Overlay.ijm");
		addExample(submenu, "Array Functions", "Array_Functions.ijm");
		addExample(submenu, "Dual Progress Bars", "Dual_Progress_Bars.ijm");
		addExample(submenu, "Grab Viridis Colormap", "Grab_Viridis_Colormap.ijm");
		addExample(submenu, "Tool", "Circle_Tool.ijm");
		submenu.addActionListener(listener);
		menu.add(submenu);
		submenu = new Menu("JavaScript");
		addExample(submenu, "Sphere", "Sphere.js");
		addExample(submenu, "Example Plot", "Example_Plot.js");
		addExample(submenu, "Semi-log Plot", "Semi-log_Plot.js");
		addExample(submenu, "Arrow Plot", "Arrow_Plot.js");
		addExample(submenu, "Process Folder", "Batch_Process_Folder.js");
		addExample(submenu, "Sine/Cosine Table", "Sine_Cosine_Table.js");
		addExample(submenu, "Non-numeric Table", "Non-numeric_Table.js");
		addExample(submenu, "Overlay", "Overlay.js");
		addExample(submenu, "Stack Overlay", "Stack_Overlay.js");
		addExample(submenu, "Dual Progress Bars", "Dual_Progress_Bars.js");
		submenu.addActionListener(listener);
		menu.add(submenu);
		submenu = new Menu("BeanShell");
		addExample(submenu, "Sphere", "Sphere.bsh");
		addExample(submenu, "Example Plot", "Example_Plot.bsh");
		addExample(submenu, "Semi-log Plot", "Semi-log_Plot.bsh");
		addExample(submenu, "Arrow Plot", "Arrow_Plot.bsh");
		addExample(submenu, "Sine/Cosine Table", "Sine_Cosine_Table.bsh");
		submenu.addActionListener(listener);
		menu.add(submenu);
		submenu = new Menu("Python");
		addExample(submenu, "Sphere", "Sphere.py");
		addExample(submenu, "Animated Gaussian Blur", "Animated_Gaussian_Blur.py");
		addExample(submenu, "Rotational Animation.py", "Rotational_Animation.py");
		addExample(submenu, "Overlay", "Overlay.py");
		submenu.addActionListener(listener);
		menu.add(submenu);
		submenu = new Menu("Java");
		addExample(submenu, "Sphere", "Sphere_.java");
		addExample(submenu, "Plugin", "My_Plugin.java");
		addExample(submenu, "Plugin Filter", "Filter_Plugin.java");
		addExample(submenu, "Plugin Frame", "Plugin_Frame.java");
		addExample(submenu, "Plugin Tool", "Prototype_Tool.java");
		submenu.addActionListener(listener);
		menu.add(submenu);
		menu.addSeparator();
		CheckboxMenuItem item = new CheckboxMenuItem("Autorun");
		menu.add(item);
		item.addItemListener(ij);
		item.setState(Prefs.autoRunExamples);
		return menu;
	}
	
	private static void addExample(Menu menu, String label, String command) {
		MenuItem item = new MenuItem(label);
		menu.add(item);
		item.setActionCommand(command);
	}

	void addOpenRecentSubMenu(Menu menu) {
		openRecentMenu = getMenu("File>Open Recent");
 		for (int i=0; i<MAX_OPEN_RECENT_ITEMS; i++) {
			String path = Prefs.getString("recent" + (i/10)%10 + i%10);
			if (path==null) break;
			MenuItem item = new MenuItem(path);
			openRecentMenu.add(item);
			item.addActionListener(ij);
		}
		menu.add(openRecentMenu);
	}

	static void addItem(Menu menu, String label, int shortcut, boolean shift) {
		if (menu==null)
			return;
		MenuItem item;
		if (shortcut==0)
			item = new MenuItem(label);
		else {
			if (shift) {
				item = new MenuItem(label, new MenuShortcut(shortcut, true));
				shortcuts.put(new Integer(shortcut+200),label);
			} else {
				item = new MenuItem(label, new MenuShortcut(shortcut));
				shortcuts.put(new Integer(shortcut),label);
			}
		}
		if (addSorted) {
			if (menu==pluginsMenu)
				addItemSorted(menu, item, userPluginsIndex);
		} else
			menu.add(item);
		item.addActionListener(ij);
	}
	
	void addPlugInItem(Menu menu, String label, String className, int shortcut, boolean shift) {
		pluginsTable.put(label, className);
		nPlugins++;
		addItem(menu, label, shortcut, shift);
	}

	static Menu addSubMenu(Menu menu, String name) {
		String value;
		String key = name.toLowerCase(Locale.US);
		int index;
 		Menu submenu=new Menu(name.replace('_', ' '));
		index = key.indexOf(' ');
		if (index>0)
			key = key.substring(0, index);
 		for (int count=1; count<100; count++) {
			value = Prefs.getString(key + (count/10)%10 + count%10);
			if (value==null)
				break;
			if (count==1)
				menu.add(submenu);
			if (value.equals("-"))
				submenu.addSeparator();
			else
				addPluginItem(submenu, value);
		}
		if (name.equals("Lookup Tables") && applet==null)
			addLuts(submenu);
		return submenu;
	}
	
	static void addLuts(Menu submenu) {
		String path = IJ.getDirectory("luts");
		if (path==null) return;
		File f = new File(path);
		String[] list = null;
		if (applet==null && f.exists() && f.isDirectory())
			list = f.list();
		if (list==null) return;
		if (IJ.isLinux()) StringSorter.sort(list);
		submenu.addSeparator();
 		for (int i=0; i<list.length; i++) {
 			String name = list[i];
 			if (name.endsWith(".lut")) {
 				name = name.substring(0,name.length()-4);
 				if (name.contains("_") && !name.contains(" "))
 					name = name.replace("_", " ");
 				MenuItem item = new MenuItem(name);
				submenu.add(item);
				item.addActionListener(ij);
				nPlugins++;
			}
		}
	}

	static void addPluginItem(Menu submenu, String s) {
		if (s.startsWith("\"-\"")) {
			// add menu separator if command="-"
			addSeparator(submenu);
			return;
		}
		int lastComma = s.lastIndexOf(',');
		if (lastComma<=0)
			return;
		String command = s.substring(1,lastComma-1);
		int keyCode = 0;
		boolean shift = false;
		if (command.endsWith("]")) {
			int openBracket = command.lastIndexOf('[');
			if (openBracket>0) {
				String shortcut = command.substring(openBracket+1,command.length()-1);
				keyCode = convertShortcutToCode(shortcut);
				boolean functionKey = keyCode>=KeyEvent.VK_F1 && keyCode<=KeyEvent.VK_F12;
				if (keyCode>0 && !functionKey)
					command = command.substring(0,openBracket);
				//IJ.write(command+": "+shortcut);
			}
		}
		if (keyCode>=KeyEvent.VK_F1 && keyCode<=KeyEvent.VK_F12) {
			shortcuts.put(new Integer(keyCode),command);
			keyCode = 0;
		} else if (keyCode>=265 && keyCode<=290) {
			keyCode -= 200;
			shift = true;
		}
		addItem(submenu,command,keyCode,shift);
		while(s.charAt(lastComma+1)==' ' && lastComma+2<s.length())
			lastComma++; // remove leading spaces
		String className = s.substring(lastComma+1,s.length());
		//IJ.log(command+"  "+className);
		if (installingJars)
			duplicateCommand = pluginsTable.get(command)!=null;
		pluginsTable.put(command, className);
		nPlugins++;
	}

	public static String getJarFileForMenuEntry(String menuEntry) {
		if (instance == null)
			return null;
		return (String)instance.menuEntry2jarFile.get(menuEntry);
	}

    /** Install a plugin located in a JAR file. */
	void installJarPlugin(String jar, String s) {
		addSorted = false;
		Menu menu;
		s = s.trim();
		if (s.startsWith("Plugins>")) {
			int firstComma = s.indexOf(',');
			if (firstComma==-1 || firstComma<=8)
				menu = null;
			else {
				String name = s.substring(8, firstComma);
				menu = getPluginsSubmenu(name);
			}
		} else if (s.startsWith("\"") || s.startsWith("Plugins")) {
			String name = getSubmenuName(jar);
			if (name!=null)
				menu = getPluginsSubmenu(name);
			else
				menu = pluginsMenu;
			addSorted = true;
		} else {
			int firstQuote = s.indexOf('"');
			String name = firstQuote<0 ? s : s.substring(0, firstQuote).trim();
			int comma = name.indexOf(',');
			if (comma >= 0)
				name = name.substring(0, comma);
			if (name.startsWith("Help>About")) // for backward compatibility
				name = "Help>About Plugins";
			menu = getMenu(name);
		}
		int firstQuote = s.indexOf('"');
		if (firstQuote==-1)
			return;
		s = s.substring(firstQuote, s.length()); // remove menu
		if (menu!=null) {
            addPluginItem(menu, s);
            addSorted = false;
        }
		String menuEntry = s;
		if (s.startsWith("\"")) {
			int quote = s.indexOf('"', 1);
			menuEntry = quote<0?s.substring(1):s.substring(1, quote);
		} else {
			int comma = s.indexOf(',');
			if (comma > 0)
				menuEntry = s.substring(0, comma);
		}
		if (duplicateCommand) {
			if (jarError==null) jarError = "";
            addJarErrorHeading(jar);
			String jar2 = (String)menuEntry2jarFile.get(menuEntry);
			if (jar2 != null && jar2.startsWith(pluginsPath))
				jar2 = jar2.substring(pluginsPath.length());
			jarError += "    Duplicate command: " + s
				+ (jar2 != null ? " (already in " + jar2 + ")"
				   : "") + "\n";
		} else
			menuEntry2jarFile.put(menuEntry, jar);
		duplicateCommand = false;
    }
    
    void addJarErrorHeading(String jar) {
        if (!isJarErrorHeading) {
                if (!jarError.equals(""))
                    jarError += " \n";
                jarError += "Plugin configuration error: " + jar + "\n";
                isJarErrorHeading = true;
            }
    }

	/** Returns the specified ImageJ menu (e.g., "File>New") or null if it is not found. */
	public static Menu getImageJMenu(String menuPath) {
		if (menus.get(menuPath)!=null)
			return getMenu(menuPath, false);
		else
			return null;
	}

	private static Menu getMenu(String menuPath) {
		return getMenu(menuPath, false);
	}

	private static Menu getMenu(String menuName, boolean readFromProps) {
		if (menuName.endsWith(">"))
			menuName = menuName.substring(0, menuName.length() - 1);
		Menu result = (Menu) menus.get(menuName);
		if (result==null) {
			int offset = menuName.lastIndexOf('>');
			if (offset < 0) {
				result = new Menu(menuName);
				if (mbar == null)
					mbar = new MenuBar();
				if (menuName.equals("Help"))
					mbar.setHelpMenu(result);
				else
					mbar.add(result);
				if (menuName.equals("Window"))
					window = result;
				else if (menuName.equals("Plugins"))
					pluginsMenu = result;
			} else {
				String parentName = menuName.substring(0, offset);
				String menuItemName = menuName.substring(offset + 1);
				Menu parentMenu = getMenu(parentName);
				result = new Menu(menuItemName);
				if (readFromProps)
					result = addSubMenu(parentMenu, menuItemName);
				else if (parentName.startsWith("Plugins"))
					addItemSorted(parentMenu, result, parentName.equals("Plugins")?userPluginsIndex:0);
				else
					parentMenu.add(result);
				if (menuName.equals("File>Open Recent"))
					openRecentMenu = result;
			}
			menus.put(menuName, result);
		}
		return result;
	}

	Menu getPluginsSubmenu(String submenuName) {
		return getMenu("Plugins>" + submenuName);
    }
    
	String getSubmenuName(String jarPath) {
		//IJ.log("getSubmenuName: \n"+jarPath+"\n"+pluginsPath);
		if (pluginsPath == null)
			return null;
		if (jarPath.startsWith(pluginsPath))
			jarPath = jarPath.substring(pluginsPath.length() - 1);
		int index = jarPath.lastIndexOf(File.separatorChar);
		if (index<0) return null;
		String name = jarPath.substring(0, index);
		index = name.lastIndexOf(File.separatorChar);
		if (index<0) return null;
		name = name.substring(index+1);
		if (name.equals("plugins")) return null;
		return name;
    }

	static void addItemSorted(Menu menu, MenuItem item, int startingIndex) {
		String itemLabel = item.getLabel();
		int count = menu.getItemCount();
		boolean inserted = false;
		for (int i=startingIndex; i<count; i++) {
			MenuItem mi = menu.getItem(i);
			String label = mi.getLabel();
			//IJ.log(i+ "  "+itemLabel+"  "+label + "  "+(itemLabel.compareTo(label)));
			if (itemLabel.compareTo(label)<0) {
				menu.insert(item, i);
				inserted = true;
				break;
			}
		}
		if (!inserted) menu.add(item);
	}

    static void addSeparator(Menu menu) {
    	menu.addSeparator();
    }


	void setupPluginsAndMacrosPaths() {
		ImageJPath = pluginsPath = macrosPath = null;
		String currentDir = Prefs.getHomeDir(); // "user.dir"
		if (currentDir==null)
			return;
		if (currentDir.endsWith("plugins"))
			ImageJPath = pluginsPath = currentDir+File.separator;
		else {
			String pluginsDir = System.getProperty("plugins.dir");
			if (pluginsDir!=null) {
				if (pluginsDir.endsWith("/")||pluginsDir.endsWith("\\"))
					pluginsDir = pluginsDir.substring(0, pluginsDir.length()-1);
				if (pluginsDir.endsWith("/plugins")||pluginsDir.endsWith("\\plugins"))
					pluginsDir = pluginsDir.substring(0, pluginsDir.length()-8);
			}
			if (pluginsDir==null)
				pluginsDir = currentDir;
			else if (pluginsDir.equals("user.home")) {
				pluginsDir = System.getProperty("user.home");
				if (!(new File(pluginsDir+File.separator+"plugins")).isDirectory()) 
					pluginsDir = pluginsDir + File.separator + "ImageJ";
				// needed to run plugins when ImageJ launched using Java WebStart
				if (applet==null)
					System.setSecurityManager(null);
				jnlp = true;
			}
			pluginsPath = pluginsDir+File.separator+"plugins"+File.separator;
			macrosPath = pluginsDir+File.separator+"macros"+File.separator;
			ImageJPath = pluginsDir+File.separator;
		}
		File f = pluginsPath!=null?new File(pluginsPath):null;
		if (f==null || !f.isDirectory()) {
			ImageJPath = currentDir+File.separator;
			pluginsPath = ImageJPath+"plugins"+File.separator;
			f = new File(pluginsPath);
			if (!f.isDirectory()) {
				String altPluginsPath = System.getProperty("plugins.dir");
				if (altPluginsPath!=null) {
					f = new File(altPluginsPath);
					if (!f.isDirectory())
						altPluginsPath = null;
					else {
						ImageJPath = f.getParent() + File.separator;
						pluginsPath = ImageJPath + f.getName() + File.separator;
						macrosPath = ImageJPath+"macros"+File.separator;
					}
				}
				if (altPluginsPath==null)
					ImageJPath = pluginsPath = null;
			}
		}
		f = macrosPath!=null?new File(macrosPath):null;
		if (f!=null && !f.isDirectory()) {
			macrosPath = currentDir+File.separator+"macros"+File.separator;
			f = new File(macrosPath);
			if (!f.isDirectory())
				macrosPath = null;
		}
		if (IJ.debugMode) {
			IJ.log("Menus.setupPluginsAndMacrosPaths");
			IJ.log("   user.dir: "+currentDir);
			IJ.log("   plugins.dir: "+System.getProperty("plugins.dir"));
			IJ.log("   ImageJPath: "+ImageJPath);
			IJ.log("   pluginsPath: "+pluginsPath);
		}
	}
		
	/** Returns a list of the plugins in the plugins menu. */
	public static synchronized String[] getPlugins() {
		File f = pluginsPath!=null?new File(pluginsPath):null;
		if (f==null || (f!=null && !f.isDirectory()))
			return null;
		String[] list = f.list();
		if (list==null)
			return null;
		Vector v = new Vector();
		jarFiles = null;
		macroFiles = null;
		for (int i=0; i<list.length; i++) {
			String name = list[i];
			boolean isClassFile = name.endsWith(".class");
			boolean hasUnderscore = name.indexOf('_')>=0;
			if (hasUnderscore && isClassFile && name.indexOf('$')<0 ) {
				name = name.substring(0, name.length()-6); // remove ".class"
				v.addElement(name);
			} else if (hasUnderscore && (name.endsWith(".jar") || name.endsWith(".zip"))) {
				if (jarFiles==null) jarFiles = new Vector();
				jarFiles.addElement(pluginsPath + name);
			} else if (validMacroName(name,hasUnderscore)) {
				if (macroFiles==null) macroFiles = new Vector();
				macroFiles.addElement(name);
			} else {
				if (!isClassFile)
					checkSubdirectory(pluginsPath, name, v);
			}
		}
		list = new String[v.size()];
		v.copyInto((String[])list);
		StringSorter.sort(list);
		return list;
	}
	
	/** Looks for plugins and jar files in a subdirectory of the plugins directory. */
	private static void checkSubdirectory(String path, String dir, Vector v) {
		if (dir.endsWith(".java"))
			return;
		File f = new File(path, dir);
		if (!f.isDirectory())
			return;
		String[] list = f.list();
		if (list==null)
			return;
		dir += "/";
		int classCount=0, otherCount=0;
		String className = null;
		for (int i=0; i<list.length; i++) {
			String name = list[i];
			boolean hasUnderscore = name.indexOf('_')>=0;
			if (hasUnderscore && name.endsWith(".class") && name.indexOf('$')<0) {
				name = name.substring(0, name.length()-6); // remove ".class"
				v.addElement(dir+name);
				classCount++;
				className = name;
				//IJ.write("File: "+f+"/"+name);
			} else if (hasUnderscore && (name.endsWith(".jar") || name.endsWith(".zip"))) {
				if (jarFiles==null) jarFiles = new Vector();
				jarFiles.addElement(f.getPath() + File.separator + name);
				otherCount++;
			} else if (validMacroName(name,hasUnderscore)) {
				if (macroFiles==null) macroFiles = new Vector();
				macroFiles.addElement(dir + name);
				otherCount++;
			} else {
				File f2 = new File(f, name);
				if (f2.isDirectory()) installSubdirectorMacros(f2, dir+name);
			}
		}
		if (Prefs.moveToMisc && classCount==1 && otherCount==0 && dir.indexOf("_")==-1)
			v.setElementAt("Miscellaneous/" + className,
				v.size() - 1);
	}
	
	/** Installs macros and scripts located in subdirectories. */
	private static void installSubdirectorMacros(File f2, String dir) {
		if (dir.endsWith("Launchers")) return;
		String[] list = f2.list();
		if (list==null) return;
		for (int i=0; i<list.length; i++) {
			String name = list[i];
			boolean hasUnderscore = name.indexOf('_')>=0;
			if (validMacroName(name,hasUnderscore)) {
				if (macroFiles==null) macroFiles = new Vector();
				macroFiles.addElement(dir+"/"+name);
			}
		}
	}
	
	private static boolean validMacroName(String name, boolean hasUnderscore) {
		return (hasUnderscore&&name.endsWith(".txt")) || name.endsWith(".ijm")
			|| name.endsWith(".js") || name.endsWith(".bsh") || name.endsWith(".py");
	}

	public void installUserPlugin(String className, boolean force) {
		int slashIndex = className.indexOf('/');
		String menuName = slashIndex < 0 ? "Plugins" : "Plugins>" +
			className.substring(0, slashIndex).replace('/', '>');
		Menu menu = getMenu(menuName);
		String command = className;
		if (slashIndex>0) {
			command = className.substring(slashIndex+1);
		}
		command = command.replace('_',' ');
		command.trim();
		boolean itemExists = (pluginsTable.get(command)!=null);
		if(force && itemExists)
			return;

		if (!force && itemExists)  // duplicate command?
			command = command + " Plugin";
		MenuItem item = new MenuItem(command);
		if(force)
			addItemSorted(menu,item,0);
		item.addActionListener(ij);
		pluginsTable.put(command, className.replace('/', '.'));
		nPlugins++;
	}

	void installPopupMenu(ImageJ ij) {
		String s;
		int count = 0;
		MenuItem mi;
		popup = new PopupMenu("");
		if (fontSize!=0)
			popup.setFont(getFont());

		while (true) {
			count++;
			s = Prefs.getString("popup" + (count/10)%10 + count%10);
			if (s==null)
				break;
			if (s.equals("-"))
				popup.addSeparator();
			else if (!s.equals("")) {
				mi = new MenuItem(s);
				mi.addActionListener(ij);
				popup.add(mi);
			}
		}
	}

	public static MenuBar getMenuBar() {
		return mbar;
	}

	public static Menu getOpenRecentMenu() {
		return openRecentMenu;
	}

	public int getMacroCount() {
		return nMacros;
	}

	public int getPluginCount() {
		return nPlugins;
	}
		
	/** Use Prefs.getImageJDir() to get the path to the ImageJ directory. */
	static String getImageJPath() {
		return ImageJPath;
	}

	/** Returns the path to the user plugins directory or
		null if the plugins directory was not found. */
	public static String getPlugInsPath() {
		return pluginsPath;
	}

	/** Returns the path to the macros directory or
		null if the macros directory was not found. */
	public static String getMacrosPath() {
		return macrosPath;
	}
        
	/** Returns the hashtable that associates commands with plugins. */
	public static Hashtable getCommands() {
		return pluginsTable;
	}
        
	/** Returns the hashtable that associates shortcuts with commands. The keys
		in the hashtable are Integer keycodes, or keycode+200 for uppercase. */
	public static Hashtable getShortcuts() {
		return shortcuts;
	}
        
	/** Returns the hashtable that associates keyboard shortcuts with macros. The keys
		in the hashtable are Integer keycodes, or keycode+200 for uppercase. */
	public static Hashtable getMacroShortcuts() {
		if (macroShortcuts==null)
			macroShortcuts = new Hashtable();
		return macroShortcuts;
	}
        
	/** Inserts one item (a non-image window) into the Window menu. */
	static synchronized void insertWindowMenuItem(Window win) {
		if (ij==null || win==null)
			return;
		String title = win instanceof Frame?((Frame)win).getTitle():((Dialog)win).getTitle();
		CheckboxMenuItem item = new CheckboxMenuItem(title);
		item.addItemListener(ij);
		int index = WINDOW_MENU_ITEMS+windowMenuItems2;
		if (windowMenuItems2>=2)
			index--;
		window.insert(item, index);
		windowMenuItems2++;
		if (windowMenuItems2==1) {
			window.insertSeparator(WINDOW_MENU_ITEMS+windowMenuItems2);
			windowMenuItems2++;
		}
	}

	/** Adds one image to the end of the Window menu. */
	static synchronized void addWindowMenuItem(ImagePlus imp) {
		if (ij==null) return;
		String name = imp.getTitle();
		String size = ImageWindow.getImageSize(imp);
		CheckboxMenuItem item = new CheckboxMenuItem(name+" "+size);
		item.setActionCommand("" + imp.getID());
		window.add(item);
		item.addItemListener(ij);
	}
	
	/** Removes the specified item from the Window menu. */
	static synchronized void removeWindowMenuItem(int index) {
		//IJ.log("removeWindowMenuItem: "+index+" "+windowMenuItems2+" "+window.getItemCount());
		if (ij==null)
			return;
		try {
			if (index>=0 && index<window.getItemCount()) {
				window.remove(WINDOW_MENU_ITEMS+index);
				if (index<windowMenuItems2) {
					windowMenuItems2--;
					if (windowMenuItems2==1) {
						window.remove(WINDOW_MENU_ITEMS);
						windowMenuItems2 = 0;
					}
				}
			}
		} catch (Exception e) {}
	}

	/** Changes the name of an item in the Window menu. */
	public static synchronized void updateWindowMenuItem(String oldLabel, String newLabel) {
		updateWindowMenuItem(null, oldLabel, newLabel);
	}

	/** Changes the name of an item in the Window menu. */
	public static synchronized void updateWindowMenuItem(ImagePlus imp, String oldLabel, String newLabel) {
		if (oldLabel==null || newLabel==null)
			return;
		int first = WINDOW_MENU_ITEMS;
		int count = window.getItemCount();
		try {  // workaround for Linux/Java 5.0/bug
			for (int i=first; i<count; i++) {
				MenuItem item = window.getItem(i);
				String label = item.getLabel();
				if (imp!=null) {  //remove size (e.g. " 24MB")
					int index = label.lastIndexOf(" ");
					if (index>-1)
						label = label.substring(0, index);
				}
				if (item!=null && label.equals(oldLabel)) {
					String size = "";
					if (imp!=null)
						size =  " " + ImageWindow.getImageSize(imp);
					item.setLabel(newLabel+size);
					return;
				}
			}
		} catch (Exception e) {}
	}
	
	/** Adds a file path to the beginning of the File/Open Recent submenu. */
	public static synchronized void addOpenRecentItem(String path) {
		if (ij==null) return;
		int count = openRecentMenu.getItemCount();
		for (int i=0; i<count; ) {
			if (openRecentMenu.getItem(i).getLabel().equals(path)) {
				openRecentMenu.remove(i);
				count--;
			} else
				i++;
		}
		if (count==MAX_OPEN_RECENT_ITEMS)
			openRecentMenu.remove(MAX_OPEN_RECENT_ITEMS-1);
		MenuItem item = new MenuItem(path);
		openRecentMenu.insert(item, 0);
		item.addActionListener(ij);
	}

	public static PopupMenu getPopupMenu() {
		return popup;
	}
	
	public static Menu getSaveAsMenu() {
		return saveAsMenu;
	}

	/** Adds a plugin based command to the end of a specified menu.
	* @param plugin			the plugin (e.g. "Inverter_", "Inverter_("arg")")
	* @param menuCode		PLUGINS_MENU, IMPORT_MENU, SAVE_AS_MENU or HOT_KEYS
	* @param command		the menu item label (set to "" to uninstall)
	* @param shortcut		the keyboard shortcut (e.g. "y", "Y", "F1")
	* @param ij				ImageJ (the action listener)
	*
	* @return				returns an error code(NORMAL_RETURN,COMMAND_IN_USE_ERROR, etc.)
	*/
	public static int installPlugin(String plugin, char menuCode, String command, String shortcut, ImageJ ij) {
		if (command.equals("")) //uninstall
			return NORMAL_RETURN;
		if (commandInUse(command))
			return COMMAND_IN_USE;
		if (!validShortcut(shortcut))
			return INVALID_SHORTCUT;
		if (shortcutInUse(shortcut))
			return SHORTCUT_IN_USE;
			
		Menu menu;
		switch (menuCode) {
			case PLUGINS_MENU: menu = pluginsMenu; break;
			case IMPORT_MENU: menu = getMenu("File>Import"); break;
			case SAVE_AS_MENU: menu = getMenu("File>Save As"); break;
			case ABOUT_MENU: menu = getMenu("Help>About Plugins"); break;
			case FILTERS_MENU: menu = getMenu("Process>Filters"); break;
			case TOOLS_MENU: menu = getMenu("Analyze>Tools"); break;
			default: return 0;
		}
		int code = convertShortcutToCode(shortcut);
		MenuItem item;
		boolean functionKey = code>=KeyEvent.VK_F1 && code<=KeyEvent.VK_F12;
		if (code==0)
			item = new MenuItem(command);
		else if (functionKey) {
			command += " [F"+(code-KeyEvent.VK_F1+1)+"]";
			shortcuts.put(new Integer(code),command);
			item = new MenuItem(command);
		} else {
			shortcuts.put(new Integer(code),command);
			int keyCode = code;
			boolean shift = false;
			if (keyCode>=265 && keyCode<=290) {
				keyCode -= 200;
				shift = true;
			}
			item = new MenuItem(command, new MenuShortcut(keyCode, shift));
		}
		menu.add(item);
		item.addActionListener(ij);
		pluginsTable.put(command, plugin);
		shortcut = code>0 && !functionKey?"["+shortcut+"]":"";
		pluginsPrefs.addElement(menuCode+",\""+command+shortcut+"\","+plugin);
		return NORMAL_RETURN;
	}
	
	/** Deletes a command installed by Plugins/Shortcuts/Add Shortcut. */
	public static int uninstallPlugin(String command) {
		boolean found = false;
		for (Enumeration en=pluginsPrefs.elements(); en.hasMoreElements();) {
			String cmd = (String)en.nextElement();
			if (cmd.contains(command)) {
				boolean ok = pluginsPrefs.removeElement((Object)cmd);
				found = true;
				break;
			}
		}
		if (found)
			return NORMAL_RETURN;
		else
			return COMMAND_NOT_FOUND;

	}
	
	public static boolean commandInUse(String command) {
		if (pluginsTable.get(command)!=null)
			return true;
		else
			return false;
	}

	public static int convertShortcutToCode(String shortcut) {
		int code = 0;
		int len = shortcut.length();
		if (len==2 && shortcut.charAt(0)=='F') {
			code = KeyEvent.VK_F1+(int)shortcut.charAt(1)-49;
			if (code>=KeyEvent.VK_F1 && code<=KeyEvent.VK_F9)
				return code;
			else
				return 0;
		}
		if (len==3 && shortcut.charAt(0)=='F') {
			code = KeyEvent.VK_F10+(int)shortcut.charAt(2)-48;
			if (code>=KeyEvent.VK_F10 && code<=KeyEvent.VK_F12)
				return code;
			else
				return 0;
		}
		if (len==2 && shortcut.charAt(0)=='N') { // numeric keypad
			code = KeyEvent.VK_NUMPAD0+(int)shortcut.charAt(1)-48;
			if (code>=KeyEvent.VK_NUMPAD0 && code<=KeyEvent.VK_NUMPAD9)
				return code;
			switch (shortcut.charAt(1)) {
				case '/': return KeyEvent.VK_DIVIDE;
				case '*': return KeyEvent.VK_MULTIPLY;
				case '-': return KeyEvent.VK_SUBTRACT;
				case '+': return KeyEvent.VK_ADD;
				case '.': return KeyEvent.VK_DECIMAL;
				default: return 0;
			}
		}
		if (len!=1)
			return 0;
		int c = (int)shortcut.charAt(0);
		if (c>=65&&c<=90) //A-Z
			code = KeyEvent.VK_A+c-65 + 200;
		else if (c>=97&&c<=122) //a-z
			code = KeyEvent.VK_A+c-97;
		else if (c>=48&&c<=57) //0-9
			code = KeyEvent.VK_0+c-48;
		return code;
	}
	
	void installStartupMacroSet() {
		if (applet!=null) {
			String docBase = ""+applet.getDocumentBase();
			if (!docBase.endsWith("/")) {
				int index = docBase.lastIndexOf("/");
				if (index!=-1)
					docBase = docBase.substring(0, index+1);
			}
			IJ.runPlugIn("ij.plugin.URLOpener", docBase+"StartupMacros.txt");
			return;
		}

		if (macrosPath==null) {
			try {
				(new MacroInstaller()).installFromIJJar("/macros/StartupMacros.txt");
			} catch (Exception e) {}
			return;
		}
		String path = macrosPath + "StartupMacros.txt";
		File f = new File(path);
		if (!f.exists()) {
			path = macrosPath + "StartupMacros.ijm";
			f = new File(path);
			if (!f.exists()) {
				(new MacroInstaller()).installFromIJJar("/macros/StartupMacros.txt");
				return;
			}
		}
		String libraryPath = macrosPath + "Library.txt";
		f = new File(libraryPath);
		boolean isLibrary = f.exists();
		try {
			MacroInstaller mi = new MacroInstaller();
			if (isLibrary) mi.installLibrary(libraryPath);
			mi.installFile(path);
			nMacros += mi.getMacroCount();
		} catch (Exception e) {}
	}
	
	static boolean validShortcut(String shortcut) {
		int len = shortcut.length();
		if (shortcut.equals(""))
			return true;
		else if (len==1)
			return true;
		else if (shortcut.startsWith("F") && (len==2 || len==3))
			return true;
		else
			return false;
	}

	/** Returns 'true' if this keyboard shortcut is in use. */
	public static boolean shortcutInUse(String shortcut) {
		int code = convertShortcutToCode(shortcut);
		if (shortcuts.get(new Integer(code))!=null)
			return true;
		else
			return false;
	}
	
	/** Set the size (in points) used for the fonts in ImageJ menus. 
		Set the size to 0 to use the Java default size. */
	public static void setFontSize(int size) {
		if (size<9 && size!=0) size = 9;
		if (size>24) size = 24;
		fontSize = size;
	}
	
	/** Returns the size (in points) used for the fonts in ImageJ menus. Returns
		0 if the default font size is being used or if this is a Macintosh. */
	public static int getFontSize() {
		return IJ.isMacintosh()?0:fontSize;
	}
	
	public static Font getFont() {
		if (menuFont==null)
			menuFont =  new Font("SanSerif", Font.PLAIN, fontSize==0?12:fontSize);
		return menuFont;
	}

	/** Called once when ImageJ quits. */
	public static void savePreferences(Properties prefs) {
		int index = 0;
		for (Enumeration en=pluginsPrefs.elements(); en.hasMoreElements();) {
			String key = "plugin" + (index/10)%10 + index%10;
			String value = (String)en.nextElement();
			prefs.put(key, value);
			index++;
		}
		int n = openRecentMenu.getItemCount();
		for (int i=0; i<n; i++) {
			String key = ""+i;
			if (key.length()==1) key = "0"+key;
			key = "recent"+key;
			prefs.put(key, openRecentMenu.getItem(i).getLabel());
		}
		prefs.put(Prefs.MENU_SIZE, Integer.toString(fontSize));
	}
	
	public static void updateImageJMenus() {
		jarFiles = macroFiles = null;
		Menus m = new Menus(IJ.getInstance(), IJ.getApplet());
		String err = m.addMenuBar();
		if (err!=null) IJ.error(err);
		m.installStartupMacroSet();
		IJ.resetClassLoader();
		IJ.runPlugIn("ij.plugin.ClassChecker", "");
		IJ.showStatus("Menus updated: "+m.nPlugins + " commands, " + m.nMacros + " macros");
	}
	
}
