/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ij.macro;

public interface MacroExtension {
  public static final int ARG_STRING = 0x01;
  public static final int ARG_NUMBER = 0x02;
  public static final int ARG_ARRAY  = 0x04;
  
  public static final int ARG_OUTPUT = 0x10;
  public static final int ARG_OPTIONAL = 0x20;

  public String handleExtension(String name, Object[] args);
  
  public ExtensionDescriptor[] getExtensionFunctions();
}
